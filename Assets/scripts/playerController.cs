﻿using UnityEngine;
using System.Collections;

public class playerController : MonoBehaviour {

	private float GRAVITY = 10;
	public GameObject[] marbles;
	public GUIText gameOverTxt;
	int frame = 0;
	int frameRate = 30;

	void Start(){
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		audio.volume = 10;
		audio.Play();
	}

	bool isInPit(GameObject obj){
		Vector3 objPos = obj.transform.position;
		if ((objPos.x < 2.6 && objPos.x > -2.6) && (objPos.z < 1.5 && objPos.z > -2.9)) {
			return true;
		} else {
			return false;
		}
	}

	void Update(){
		marbles = GameObject.FindGameObjectsWithTag ("marble");
		bool isGameOver = true;
		foreach(GameObject marble in marbles){
			if(!isInPit(marble)){
				isGameOver = false;
			}

			float mag = marble.rigidbody.velocity.magnitude;
			if(mag > 2){
				audio.pitch = mag;
			}else{
				audio.pitch = 0;
			}
			//Debug.Log(marble.rigidbody.angularVelocity);
		}

		//Debug.Log(marbles[0].rigidbody.velocity + " " + marbles[0].rigidbody.angularVelocity);

		if (isGameOver) {
			gameOverTxt.text = "GAME OVER";
			Debug.Log ("game is over");
		} else {
			gameOverTxt.text = "";
		}


		/*
		 * if the velocity is greater than a particular value then play the sound. If sound is not playing start
		 * if the velocity is less than a particular value then stop the sound if playing.
		*/

	}

	void FixedUpdate(){
		Vector3 originalAcc = new Vector3 (Input.acceleration.x, Input.acceleration.z, Input.acceleration.y);
		if (frame == frameRate) {
			frame = 0;
			Debug.Log (originalAcc);
		}
		frame++;
		Vector3 adjustedAcc = originalAcc * (GRAVITY);
		rigidbody.AddForce (adjustedAcc);

	}
}
